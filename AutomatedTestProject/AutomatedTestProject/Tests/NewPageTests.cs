﻿using AutomatedTestProject.Actions;
using Framework.Utilities;
using NUnit.Framework;

namespace AutomatedTestProject.Tests
{
    //Test class. Does and verifies 3 scenarios
    [TestFixture]
    public class NewPageTests : BaseClass
    {
        private string existingSpace = "Automated Test";
        string expectedPageTitle = "New page";

        [OneTimeSetUp]
        public void SetUp()
        {
            //Set Up  
        }

        [Test, Order(1)]
        public void ShouldCreateANewPage()
        {
            var actualPageTitle = new LoginPageAction()
            .LoginAs(Config.UserName, Config.Password)
            .NavigateToSpace(existingSpace).GoToAllPageInSpace().CreateANewPage("Blank page", expectedPageTitle)
            .PublishPage().GetPageTitle();

            Assert.True(actualPageTitle == expectedPageTitle, "Page was not created");
        }

        [Test, Order(2)]
        public void ShouldSetPageRestrictions()
        {
            new LoginPageAction().LoginAs(Config.UserName, Config.Password).NavigateToSpace(existingSpace).
            GoToAllPageInSpace().GoToSpecificPage(expectedPageTitle)
            .SetRestrictionsOnPage();
            //Assert restirctions on the page
        }

        [Test, Order(3)]
        public void ShouldDeleteAPage()
        {
            var loginPage = new LoginPageAction();
            loginPage.LoginAs(Config.UserName, Config.Password).NavigateToSpace(existingSpace).GoToAllPageInSpace()
            .GoToSpecificPage(expectedPageTitle).DeletePage();
            //Assert that page is not found in the list of pages in the automated test space
        }

        [TearDown]
        public void TearDown()
        {
            Quit();
        }
    }
}
