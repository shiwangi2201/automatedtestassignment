﻿using Framework.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Pages
{
    //Class containing webelements of login page
    public class LoginPageElements: BaseClass
    {
        [FindsBy(How = How.Id, Using = "username")]
        protected IWebElement UserName { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        protected IWebElement Password { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='button-text first']")]
        protected IWebElement Next { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='button-text second']")]
        protected IWebElement Login { get; set; }
    }
}
