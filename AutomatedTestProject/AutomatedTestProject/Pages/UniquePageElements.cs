﻿using Framework.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Pages
{
    //Class containing webelements of one unique page in application
    public class UniquePageElements : BaseClass
    {
        [FindsBy(How = How.CssSelector, Using = "#title-text")]
        protected IWebElement PageTitle { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#action-menu-link")]
        protected IWebElement ActionMenu { get; set; }
        
        [FindsBy(How = How.CssSelector, Using = "#action-remove-content-link")]
        protected IWebElement DeleteLink { get; set; }
        
        [FindsBy(How = How.CssSelector, Using = "#confirm")]
        protected IWebElement OkButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#content-metadata-page-restrictions")]
        protected IWebElement EditRestrictions { get; set; }
    }
}
