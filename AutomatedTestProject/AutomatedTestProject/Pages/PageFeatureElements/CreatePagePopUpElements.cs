﻿using Framework.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Pages.PageFeatureElements
{
    //Class containing webelements of the popup where we choose template to create a new page
    public class CreatePagePopUpElements : BaseClass
    {
        [FindsBy(How = How.XPath, Using = "//button[text()='Create']")]
        protected IWebElement CreateButton { get; set; }

        protected IWebElement GetTemplateElement(string templateName)
        {
            return 
            Driver.FindElement(By.XPath("//div[@class='template-name'][contains(text(),'" + templateName + "')]"));
        }
    }
}
