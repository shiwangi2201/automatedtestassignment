﻿using Framework.Utilities;
using OpenQA.Selenium;

namespace AutomatedTestProject.Pages.PageFeatureElements
{
    //Class containing webelements of left navigation page on the landing page after login
    public class DashboardPageSideBarElements : BaseClass
    {
        protected IWebElement GetSpaceElement(string spaceName)
        {
            return
                Driver.FindElement(By.XPath("//*[@id='confluence-left-nav']//div[@class='dzHTbl']" +
                                            "[contains(text(),'" + spaceName + "')]"));
        }

    }
}
