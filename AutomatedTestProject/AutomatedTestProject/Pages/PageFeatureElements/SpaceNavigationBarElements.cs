﻿using Framework.Utilities;
using OpenQA.Selenium;

namespace AutomatedTestProject.Pages.PageFeatureElements
{
    public class SpaceNavigationBarElements: BaseClass
    {
        //Class containing webelements of the left navigation bar in landing page of a space
        protected IWebElement GetModuleElementInSideBar(string moduleName)
        {
            return
                Driver.FindElement(By.XPath("//*[@id='confluence-left-nav']//div[text()= '" +
                                            moduleName + "']/ancestor::button"));


        }
    }
}
