﻿using Framework.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Pages.PageFeatureElements
{
    //Class containing webelements of the pop where restrictions on a page is set
    public class RestrictionsPopUpElements : BaseClass
    {
        [FindsBy(How = How.CssSelector, Using = ".restrictions-dialog-option")]
        protected IWebElement RestrictionsDropDown  { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#page-restrictions-dialog-save-button")]
        protected IWebElement SaveButton { get; set; }

    }
}
