﻿using Framework.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Pages
{
    //Class containing webelements of one unique page in edit mode
    public class EditUniquePageElements: BaseClass
    {
        [FindsBy(How = How.CssSelector, Using = "#rte-button-publish")]
        protected IWebElement PublishPageButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "[name='title']")]
        protected IWebElement PageTitleEdit { get; set; }
        }
}
