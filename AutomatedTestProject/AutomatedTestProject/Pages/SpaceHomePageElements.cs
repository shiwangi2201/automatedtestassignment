﻿using Framework.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Pages
{
    //Class containing webelements of the home page of a space
    public class SpaceHomePageElements : BaseClass
    {
        [FindsBy(How = How.CssSelector,Using = ".SpaceHeader_SpaceHeader_1vt > Div > button")]
        protected IWebElement CreatePageButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "[title='New page']")]
        protected IWebElement Page { get; set; }
    }
}
