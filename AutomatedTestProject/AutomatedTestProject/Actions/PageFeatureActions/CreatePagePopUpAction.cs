﻿using AutomatedTestProject.Pages.PageFeatureElements;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions.PageFeatureActions
{
    //Class containing actions on the popup where we choose template to create a new page
    public class CreatePagePopUpAction : CreatePagePopUpElements
    {
        public CreatePagePopUpAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public CreatePagePopUpAction SelectTemplate(string templateName)
        {
            GetTemplateElement(templateName).Click();
            return this;
        }

        public EditUniquePageAction ApplyTemplateOnNewPage()
        {
            CreateButton.Click();
            return new EditUniquePageAction();
        }
    }
}
