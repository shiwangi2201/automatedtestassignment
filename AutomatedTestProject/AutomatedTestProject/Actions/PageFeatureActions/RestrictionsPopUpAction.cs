﻿using AutomatedTestProject.Pages.PageFeatureElements;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions.PageFeatureActions
{
    //Class containing actions on the pop where restrictions on a page is set
    public class RestrictionsPopUpAction : RestrictionsPopUpElements
    {
        public RestrictionsPopUpAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public RestrictionsPopUpAction ChangeRestrictions()
        {
            RestrictionsDropDown.Click();
            return this;
        }

        public UniquePageAction ApplyRestrictions()
        {
            SaveButton.Click();
            return new UniquePageAction();
        }
    }
}
