﻿using System;
using AutomatedTestProject.Pages.PageFeatureElements;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions.PageFeatureActions
{
    //Class containing actions on the left navigation bar in landing page of a space
    public class SpaceNavigationBarAction:SpaceNavigationBarElements
    {
        public SpaceNavigationBarAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public void GoToModuleInNavBar(string moduleNameInNavBar)
        {
            try
            {
                GetModuleElementInSideBar(moduleNameInNavBar).Click();
            }
            catch (Exception e)
            {
              Console.WriteLine("Link on left navigation bar not found. Please provide correct link name " + e);     
            }
            
        }
    }
}
