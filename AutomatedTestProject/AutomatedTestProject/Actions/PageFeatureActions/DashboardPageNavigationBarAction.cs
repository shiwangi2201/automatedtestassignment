﻿using AutomatedTestProject.Pages.PageFeatureElements;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions.PageFeatureActions
{
    //Class containing actions on left navigation page on the landing page after login
    public class DashboardPageNavigationBarAction : DashboardPageSideBarElements
    {
        public DashboardPageNavigationBarAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public void NavigateToSpaceHomePage(string spaceName)
        {
            GetSpaceElement(spaceName).Click();
        }
    }
}
