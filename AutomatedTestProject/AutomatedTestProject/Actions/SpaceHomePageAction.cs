﻿using System.Threading;
using AutomatedTestProject.Actions.PageFeatureActions;
using AutomatedTestProject.Pages;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions
{
    //Class containing actions on the home / landing page of a space
    public class SpaceHomePageAction : SpaceHomePageElements
    {
        public SpaceHomePageAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public EditUniquePageAction CreateANewPage(string template, string pageTitle)
        {
            CreatePage().SelectTemplate(template).ApplyTemplateOnNewPage().
            EditPageTitle(pageTitle);
            return new EditUniquePageAction();
        }

        public UniquePageAction GoToSpecificPage(string pageName)
        {
            Page.Click();
            return new UniquePageAction();
        }

        private CreatePagePopUpAction CreatePage()
        {
            Thread.Sleep(5000);
            CreatePageButton.Click();
            return new CreatePagePopUpAction();
        }

        public SpaceHomePageAction GoToAllPageInSpace()
        {
            var sidebar = new SpaceNavigationBarAction();
            sidebar.GoToModuleInNavBar("Pages");
            return this;
        }

        

    }
}
