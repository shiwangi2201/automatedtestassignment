﻿using AutomatedTestProject.Pages;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions
{
    //Class containing actions on one unique page in edit mode
    public class EditUniquePageAction : EditUniquePageElements
    {

        public EditUniquePageAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public EditUniquePageAction EditPageTitle(string pageTitle)
        {
            PageTitleEdit.SendKeys(pageTitle);
            return this;
        }

        public UniquePageAction PublishPage()
        {
            PublishPageButton.Submit();
            var alert = Driver.SwitchTo().Alert();
            alert.Accept();
            return new UniquePageAction();
        }
    }
}
