﻿using AutomatedTestProject.Pages;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions
{
    //Class containing actions on login page
    class LoginPageAction : LoginPageElements
    {
        public LoginPageAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public DashboardPageAction LoginAs(string username, string password)
        {
            NavigateToApp();
            TypeUsername(username);
            GoNext();
            TypePassword(password);
            return SubmitLogin();
        }

        private void NavigateToApp()
        {
            Driver.Navigate().GoToUrl(Config.AppUrl);
        }
        private void TypeUsername(string username)
        {
            UserName.SendKeys(username);
        }

        private void TypePassword(string password)
        {
            Password.SendKeys(password);
        }

        private void GoNext()
        {
            Next.Submit();
        }

        private DashboardPageAction SubmitLogin()
        {
            Login.Submit();
            return new DashboardPageAction();
        }
    }
}
