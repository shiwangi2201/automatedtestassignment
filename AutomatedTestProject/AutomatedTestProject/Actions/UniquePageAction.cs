﻿using AutomatedTestProject.Actions.PageFeatureActions;
using AutomatedTestProject.Pages;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions
{
    //Class containing action on a unique page
    public class UniquePageAction : UniquePageElements
    {
        public UniquePageAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public void DeletePage()
        {
            ActionMenu.Click();
            DeleteLink.Click();
            OkButton.Submit();
        }

        public string GetPageTitle()
        {
            return (PageTitle.Text);
        }

        private RestrictionsPopUpAction OpenRestrictionsPopUp()
        {
            EditRestrictions.Click();
            return new RestrictionsPopUpAction();
        }

        public UniquePageAction SetRestrictionsOnPage()
        {
            return OpenRestrictionsPopUp().ChangeRestrictions().ApplyRestrictions();
        }
    }
}
