﻿using AutomatedTestProject.Actions.PageFeatureActions;
using AutomatedTestProject.Pages.PageFeatureElements;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomatedTestProject.Actions
{
    //Class containing actions on the landing page after login
    public class DashboardPageAction : DashboardPageSideBarElements
    {
        public DashboardPageAction()
        {
            PageFactory.InitElements(Driver, this);
        }

        public SpaceHomePageAction NavigateToSpace(string spaceName)
        {
            new DashboardPageNavigationBarAction().NavigateToSpaceHomePage(spaceName);
            return new SpaceHomePageAction();
        }
    }
}
