﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Framework.Utilities
{
    //Singleton class where webdriver is assigned only once during the course of each test.
    //Lowest layer where webdriver is instantiated with a page load time out and implicit wait timeout
    public class DriverUtils
    {
        private static IWebDriver _driver;
        private static DriverUtils _instance;

        protected DriverUtils()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromMilliseconds(20000);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(10000);
        }

        public static DriverUtils Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DriverUtils();
                }
                return _instance;
            }
        }

        public IWebDriver GetDriver()
        {
            return _driver;
        }

        public void QuitDriver()
        {
            _driver.Quit();
        }
    }
}
