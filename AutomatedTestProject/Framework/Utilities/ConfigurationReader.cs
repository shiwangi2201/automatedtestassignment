﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Framework.Utilities
{
    public static class ConfigurationReader
    {
        //Using execution environment, tests can be integrated with any CI tool.
        //Using below logic, tests can be run on any environment, provided it has a config

        private const string ExecutionEnvironment = "EXECUTION_ENVIRONMENT";

        public static Configuration LoadConfiguration(string environmentName)
        {
            if (environmentName == string.Empty)
            {
                throw new ArgumentException("Environment name should be specified");
            }
            var configFileName = string.Format("Framework.EnvironmentConfigurations.{0}.json", environmentName);
            using (var enviromentStream = typeof(ConfigurationReader).Assembly.GetManifestResourceStream(configFileName))
            {
                if (enviromentStream == null)
                    throw new InvalidOperationException("Failed to load configuration file - is the manifest resource stream location correctly set?");

                using (var reader = new StreamReader(enviromentStream))
                {
                    return JsonConvert.DeserializeObject<Configuration>(reader.ReadToEnd());
                }
            }
        }

        public static Configuration LoadConfiguration()
        {
            var env = Environment.GetEnvironmentVariable(ExecutionEnvironment);
            if (string.IsNullOrEmpty(env))
                env = "Local";
            return LoadConfiguration(env);
        }
    }
}
