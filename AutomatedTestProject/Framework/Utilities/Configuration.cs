﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Framework.Utilities
{
    //Reads the configuration provoded in Local.json
    public class Configuration
    {
        [JsonProperty("appUrl")]
        public string AppUrl { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("timeOut")]
        public double TimeOut { get; set; }

        [JsonProperty("template")]
        public List<string> Template { get; set; }

        [JsonProperty("blankPage")]
        public string BlankPage { get; set; }
    }
}
