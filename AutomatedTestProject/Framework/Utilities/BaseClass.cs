﻿using OpenQA.Selenium;

namespace Framework.Utilities
{
    //The only class which interacts with the webdriver. 
    //All pages interact with this class for driver instance and configuration
    public class BaseClass
    {
        protected readonly IWebDriver Driver;
        protected readonly Configuration Config;
        protected BaseClass()
        {
            Config = ConfigurationReader.LoadConfiguration();
            Driver = DriverUtils.Instance.GetDriver();
        }

        public void Quit()
        {
            DriverUtils.Instance.QuitDriver();
        }
    }
}
