# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repo comtains test automation project on confluence.
* No DB connection is required.

### How do I get set up? ###

* Check out repo. Open AutomatedTestProject.sln. Compile. Restore Nuget packages and run tests.
* All packages to be installed are configured in packages.config. You should open the C# solution (preferably in Visual Studio), right click on the AutomatedTestProject and click on "Managed Nuget Repositories". It will ensure that all your packages e.g. Selenium webdriver, Newtonsoft etc are downloaded and installed.
* Application url, username, password etc. is coming from Local.json config file. Tests can be run for any user, go to Local.json and change the username and password.
* Assembly references are already in place. No extra effort required.
* First Compile the solution, Restore the nuget packages, Go to "NewPageTests.cs". Click on the green icon on the left side of the test "ShouldCreateANewPage". Click on Run test.

### Features ###
* Webdriver instance is instantiated only once for each test in Base.cs class which interacts with the Singleton class instance DriverUtils.cs.
* Page object model has been followed. There are Tests which interact with Actions and Actions interact with their respective pages elements.
* This solution can have multiple configuration files one for each environment, so that same tests can run on multiple envs in a CI

### Assumptions: ###
* Tests assume that there is already a Space created in Confluence called "Automated Test" inside which all tests are run. Ideally the creation of a space should also be done in the data set up of a test, but, here I have limited the scope of this assignment due to time constraint.

### Who do I talk to? ###

* Repo owner or admin : Shiwangi Prasad at shiwangi2201@gmail.com
